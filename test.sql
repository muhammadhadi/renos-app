/*
 Navicat Premium Data Transfer

 Source Server         : LocalDB
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 17/04/2022 01:34:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `brand_type_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_brand_type_id`(`brand_type_id`) USING BTREE,
  CONSTRAINT `fk_brand_type_id` FOREIGN KEY (`brand_type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES (1, 'Suzuki', 2);
INSERT INTO `brand` VALUES (2, 'Honda', 2);
INSERT INTO `brand` VALUES (4, 'Toyota', 1);
INSERT INTO `brand` VALUES (5, 'Daihatsu', 1);
INSERT INTO `brand` VALUES (6, 'Nissan', 1);
INSERT INTO `brand` VALUES (7, 'Yamaha', 2);

-- ----------------------------
-- Table structure for series
-- ----------------------------
DROP TABLE IF EXISTS `series`;
CREATE TABLE `series`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `series_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `series_brand_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_series_brand_id`(`series_brand_id`) USING BTREE,
  CONSTRAINT `fk_series_brand_id` FOREIGN KEY (`series_brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series
-- ----------------------------
INSERT INTO `series` VALUES (1, 'avanza', 4);
INSERT INTO `series` VALUES (2, 'innova', 4);
INSERT INTO `series` VALUES (3, 'xenia', 5);
INSERT INTO `series` VALUES (4, 'ayla', 5);
INSERT INTO `series` VALUES (5, 'vario', 2);
INSERT INTO `series` VALUES (6, 'cbr', 2);
INSERT INTO `series` VALUES (7, 'mio', 7);
INSERT INTO `series` VALUES (8, 'r15', 7);
INSERT INTO `series` VALUES (9, 'spin', 1);
INSERT INTO `series` VALUES (10, 'satria', 1);
INSERT INTO `series` VALUES (11, 'juke', 6);
INSERT INTO `series` VALUES (12, 'livina', 6);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (1, 'Mobil');
INSERT INTO `type` VALUES (2, 'Motor');

SET FOREIGN_KEY_CHECKS = 1;
