<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mobil extends Model
{
    use HasFactory;

    public function get_mobil(){
        $result = [];
        $result['brand'] = DB::table('brand')->where('brand_type_id', "=", 1)->get();
        $result['kendaraan'] = DB::table('series')
        ->join('brand', 'series.series_brand_id', '=', 'brand.id')
        ->join('type', 'brand.brand_type_id', '=', 'type.id')
        ->where('type.id', "=", 1)->get();
        // dd($result);
        return $result;
    }
    public function get_mobil_with_brand($brand_name){
        $result['brand'] = DB::table('brand')->where('brand_type_id', "=", 1)->get();
        $result['kendaraan'] = DB::table('series')
        ->join('brand', 'series.series_brand_id', '=', 'brand.id')
        ->join('type', 'brand.brand_type_id', '=', 'type.id')
        ->where('brand_name','=',$brand_name)
        ->where('type.id', "=", 1)->get();
        return $result;
    }
    public function get_mobil_with_brand_and_series($brand_name,$series_name){
        $result['kendaraan'] = DB::table('series')
        ->join('brand', 'series.series_brand_id', '=', 'brand.id')
        ->join('type', 'brand.brand_type_id', '=', 'type.id')
        ->where('brand_name','=',$brand_name)
        ->where('series_name','=',$series_name)
        ->where('type.id', "=", 1)->get();
        return $result;
    }
}
