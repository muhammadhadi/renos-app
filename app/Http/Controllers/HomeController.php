<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mobil as Mobil;
use App\Models\Motor as Motor;


class HomeController extends Controller
{
    public function index(){
        $mbl = new Mobil;
        $mtr = new Motor;
        $data = [];
        $data['mobil'] = $mbl->get_mobil();
        $data['motor'] = $mtr->get_motor();
        // dd($data);
        return view('welcome',['data' => $data]);
    }
}