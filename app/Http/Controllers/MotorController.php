<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motor as Motor;


class MotorController extends Controller
{
    //
    public function index(){
        $motor_model = new Motor;
        $data = $motor_model->get_motor();
        // dd($data['series']);
        $data['type'] = 'motor';
        $data['endpoint'] = 'kendaraan';
        return view('result',['data' => $data]);
    }
    public function brand($brand){
        $motor_model = new Motor;
        $data = $motor_model->get_motor_with_brand($brand);
        $data['type'] = 'motor';
        $data['brand'] = $brand;
        $data['endpoint'] = 'brand';
        // dd($data);

        return view('result',['data' => $data]);
    }
    public function series($brand,$series){
        $motor_model = new Motor;
       
        $data = $motor_model->get_motor_with_brand_and_series($brand,$series);
        $data['type'] = 'motor';
        $data['brand'] = $brand;
        $data['series'] = $series;
        $data['endpoint'] = 'series';
        // dd($data);
        return view('result',['data' => $data]);
    }
}
