<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mobil as Mobil;


class MobilController extends Controller
{
    //
    public function index(){
        $mobil_model = new Mobil;
        $data = $mobil_model->get_mobil();
        // dd($data['series']);
        $data['type'] = 'Mobil';
        $data['endpoint'] = 'kendaraan';
        
        return view('result',['data' => $data]);
    }
    public function brand($brand){
        $brand = strtolower($brand);
        $mobil_model = new Mobil;
        $data = $mobil_model->get_mobil_with_brand($brand);
        $data['type'] = 'Mobil';
        $data['brand'] = ucfirst($brand);
        $data['endpoint'] = 'brand';
        // dd($data);

        return view('result',['data' => $data]);
    }
    public function series($brand,$series){
        $mobil_model = new Mobil;
        $brand = strtolower($brand);
        $series = strtolower($series);
        $data = $mobil_model->get_mobil_with_brand_and_series($brand,$series);
        $data['type'] = 'mobil';
        $data['brand'] =  ucfirst($brand);
        $data['series'] =  ucfirst($series);
        $data['endpoint'] = 'series';
        // dd($data);
        return view('result',['data' => $data]);
    }
}
