<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MobilController;
use App\Http\Controllers\MotorController;
use App\Http\Controllers\HomeController;

 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

// Route::get('/mobil',function(){
//     return view('welcome');
// });

Route::get('/mobil', [MobilController::class, 'index']);
Route::get('/mobil/{brand}', [MobilController::class, 'brand']);
Route::get('/mobil/{brand}/{series}', [MobilController::class, 'series']);

Route::get('/motor', [MotorController::class, 'index']);
Route::get('/motor/{brand}', [MotorController::class, 'brand']);
Route::get('/motor/{brand}/{series}', [MotorController::class, 'series']);
