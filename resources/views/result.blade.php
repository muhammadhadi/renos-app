<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Renos | Results</title>
</head>
<body>
    {{$data['type']}} <br>
    
    @if ($data['endpoint'] == 'kendaraan')     
        @foreach ($data['brand'] as $brand_item)
            {{$data['type'] . '/' . $brand_item->brand_name}} <br>
            @foreach ($data['kendaraan'] as $item)
                @if ($item->series_brand_id == $brand_item->id)
                {{$data['type'] . '/' . $brand_item->brand_name .'/'. $item->series_name}}<br>
                @endif
            @endforeach
        @endforeach  
    @else
        {{$data['type'].'/'.$data['brand']}} <br>
        @foreach ($data['kendaraan'] as $item)
            {{$data['type'] . '/' . $item->brand_name .'/'. $item->series_name}}<br>
        @endforeach
    @endif
   
    
   
</body>
</html>